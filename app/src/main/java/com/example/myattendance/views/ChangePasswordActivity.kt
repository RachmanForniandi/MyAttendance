package com.example.myattendance.views

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myattendance.R
import com.example.myattendance.databinding.ActivityChangePasswordBinding
import com.example.myattendance.databinding.ActivityForgotPasswordBinding

class ChangePasswordActivity : AppCompatActivity()  {
    private lateinit var binding: ActivityChangePasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChangePasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
        onClick()

    }

    private fun onClick() {
        binding.tbChangePassword.setNavigationOnClickListener{
            finish()
        }
    }

    private fun init(){
        setSupportActionBar(binding.tbChangePassword)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}