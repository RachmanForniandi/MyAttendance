package com.example.myattendance.views.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myattendance.R
import com.example.myattendance.databinding.FragmentAttendanceBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class AttendanceFragment : Fragment(),OnMapReadyCallback {
    private var mapAttendance: SupportMapFragment?= null
    private var map: GoogleMap? = null

    private var binding:FragmentAttendanceBinding?= null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding=FragmentAttendanceBinding.inflate(inflater, container, false)
        return binding?.root
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initiateMaps()

    }

    private fun initiateMaps() {
        mapAttendance = childFragmentManager.findFragmentById(R.id.map_attendance)as SupportMapFragment
        mapAttendance?.getMapAsync(this)
    }

    override fun onMapReady(gMap: GoogleMap?) {
        map = gMap
        val sydney = LatLng(-33.852,151.211)
        map?.addMarker(
                MarkerOptions()
                        .position(sydney)
                        .title("Marker in Sydney")
        )
        map?.animateCamera(CameraUpdateFactory.zoomTo(20f))
        map?.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}